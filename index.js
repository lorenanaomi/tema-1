
const FIRST_NAME = "Lorena";
const LAST_NAME = "Spinu";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function numberParser(param) {

   
    if((typeof(param)=="number")&&(param>Number.MAX_SAFE_INTEGER||param<Number.MIN_SAFE_INTEGER)||(param==Infinity||param== -Infinity))
        {
    return NaN;
        }
        else{
    return parseInt(param);
    }
    
     
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

